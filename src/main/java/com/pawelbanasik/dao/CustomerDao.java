package com.pawelbanasik.dao;

import java.util.List;

import com.pawelbanasik.domain.Customer;

public interface CustomerDao {

	public List<Customer> getCustomers();

	public void saveCustomer(Customer customer);

	public Customer getCustomers(int id);

	public void deletUser(int id);
	
}
