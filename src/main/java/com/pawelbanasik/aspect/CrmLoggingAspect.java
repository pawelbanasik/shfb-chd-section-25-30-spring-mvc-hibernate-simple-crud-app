package com.pawelbanasik.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CrmLoggingAspect {

	private Logger myLogger = Logger.getLogger(getClass().getName());

	@Pointcut("execution(* com.pawelbanasik.controller.*.*(..))")
	private void forControllerPackage() {
	}

	@Pointcut("execution(* com.pawelbanasik.service.*.*(..))")
	private void forServicePackage() {
	}

	@Pointcut("execution(* com.pawelbanasik.dao.*.*(..))")
	private void forDaoPackage() {
	}

	@Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
	private void forAppFlow() {
	}

	@Before("forAppFlow()")
	public void before(JoinPoint joinPoint) {
		String method = joinPoint.getSignature().toShortString();
		myLogger.info("====> in @Before: calling method: " + method);

		Object[] args = joinPoint.getArgs();

		for (Object tempArgs : args) {
			myLogger.info("====> argument: " + tempArgs);
		}
	}

	@AfterReturning(pointcut="forAppFlow()", returning= "theResult")
	public void afterReturning(JoinPoint joinPoint, Object theResult) {
		String method = joinPoint.getSignature().toShortString();
		myLogger.info("====> in @AfterReturning: calling method: " + method);
		
		myLogger.info("====> result: " + theResult);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
